package Home.home_six_two;

public class Calculator 
{
    
	public static int add(int a, int b) {
		return a + b;
	}
	
	public static int substract(int a, int b) {
		return a - b;
	}
	
	public static int multiply(int a, int b) {
		return a * b;
	}
	
	public static int divide(int a, int b) {
		int result;
		try {
			result = a/b;
		} catch (ArithmeticException ex) {
			result = 0;
			}
		return result;
	}
}
