package Home.home_six_two;

import org.testng.annotations.DataProvider;

public class ProviderData {

	 @DataProvider
	    public Object[][] getData() {

	        return new Object[][]{{4, 5}, {0, 3}, {2, 1}};
	    }
}
