package Home.home_six_two;

import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class DivideTest {
	
	@Test
	public void testDivide() {
		Assert.assertEquals(Calculator.divide(6, 3), 2);
	}
 
	@Parameters({"d1","d2"})
	@Test
	public void testDivideParam(int d1, int d2) {
		Assert.assertEquals(Calculator.divide(d1, d2), d1 / d2);
	}
	
	@Test
	public void testDivideSoftAssert() {
		SoftAssert sa = new SoftAssert();
		System.out.println("SoftAssert is going to be run");
		sa.assertEquals(Calculator.divide(1, 2), 3);
		System.out.println("Finish");
	}
	
	@Test (dataProvider = "getData", dataProviderClass = ProviderData.class)
	public void testDivideDataProvider(int a, int b) {
		Assert.assertEquals(Calculator.divide(a, b), a / b);
		
	}
}
