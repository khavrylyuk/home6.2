package Home.home_six_two;

import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class SubstractTest {
	
	@Test
	public void testSubstract() {
		Assert.assertEquals(Calculator.substract(5, 3), 2);
	}
 
	@Parameters({"s1","s2"})
	@Test
	public void testSubstractParam(int s1, int s2) {
		Assert.assertEquals(Calculator.substract(s1, s2), s1 - s2);
	}
	
	@Test
	public void testSubstractSoftAssert() {
		SoftAssert sa = new SoftAssert();
		System.out.println("SoftAssert is going to be run");
		sa.assertEquals(Calculator.substract(2, 1), 1);
		System.out.println("Finish");
	}
	
	@Test (dataProvider = "getData", dataProviderClass = ProviderData.class)
	public void testSubstractDataProvider(int a, int b) {
		Assert.assertEquals(Calculator.substract(a, b), a - b);
		
	}
}
