package Home.home_six_two;

import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class AddTest {
	
	@Test
	public void testAdd() {
		Assert.assertEquals(Calculator.add(2, 3), 5);
	}
 
	@Parameters({"p1","p2"})
	@Test
	public void testAddParam(int p1, int p2) {
		Assert.assertEquals(Calculator.add(p1, p2), p1 + p2);
	}
	
	@Test
	public void testAddSoftAssert() {
		SoftAssert sa = new SoftAssert();
		System.out.println("SoftAssert is going to be run");
		sa.assertEquals(Calculator.add(1, 2), 3);
		System.out.println("Finish");
	}
	
	@Test (dataProvider = "getData", dataProviderClass = ProviderData.class)
	public void testAddDataProvider(int a, int b) {
		Assert.assertEquals(Calculator.add(a, b), a + b);
		
	}
}
