package Home.home_six_two;

import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class MultiplyTest {
	
	@Test
	public void testMultiply() {
		Assert.assertEquals(Calculator.multiply(4, 5), 20);
	}
 
	@Parameters({"m1","m2"})
	@Test
	public void testMultiplyParam(int m1, int m2) {
		Assert.assertEquals(Calculator.multiply(m1, m2), m1 * m2);
	}
	
	@Test
	public void testMultiplySoftAssert() {
		SoftAssert sa = new SoftAssert();
		System.out.println("SoftAssert is going to be run");
		sa.assertEquals(Calculator.multiply(1, 2), 3);
		System.out.println("Finish");
	}
	
	@Test (dataProvider = "getData", dataProviderClass = ProviderData.class)
	public void testMultiplyDataProvider(int a, int b) {
		Assert.assertEquals(Calculator.multiply(a, b), a * b);
		
	}
}
